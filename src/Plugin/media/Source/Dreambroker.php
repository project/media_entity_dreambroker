<?php

namespace Drupal\media_entity_dreambroker\Plugin\media\Source;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;
use Drupal\media\MediaTypeInterface;
use Drupal\media\MediaSourceFieldConstraintsInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a media source plugin for Dream Broker resources.
 *
 * @MediaSource(
 *   id = "dreambroker",
 *   label = @Translation("Dream Broker"),
 *   description = @Translation("Provides business logic and metadata for Dream Broker."),
 *   allowed_field_types = {"string", "string_long", "link"},
 *   default_thumbnail_filename = "dreambroker.png"
 * )
 */
class Dreambroker extends MediaSourceBase implements MediaSourceFieldConstraintsInterface {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs a new class instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity field manager service.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type plugin manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, FieldTypePluginManagerInterface $field_type_manager, ConfigFactoryInterface $config_factory, FileSystemInterface $fileSystem) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_field_manager, $field_type_manager, $config_factory);
    $this->fileSystem = $fileSystem;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('config.factory'),
      $container->get('file_system')
    );
  }

  /**
   * List of validation regular expressions.
   *
   * @var array
   */
  public static $validationRegexp = [
    '#(?:https?:\/\/)?(?:www\.)?(?:dreambroker\.com\/(?:channel\/(?<channelid>[a-z0-9]{8})\/(?<videoid>[a-z0-9]{8})))#' => 'id',
  ];

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    $fields = [
      'channelid' => $this->t('Dream Broker channel id'),
      'videoid' => $this->t('Dream Broker video id'),
    ];

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {
    $matches = $this->matchRegexp($media);

    if (!$matches['channelid'] && !$matches['videoid']) {
      return FALSE;
    }

    if (!empty($matches[$attribute_name])) {
      return $matches[$attribute_name];
    }

    // Special case to download a thumbnail locally.
    if ($attribute_name == 'thumbnail_uri') {
      $directory = $this->configFactory->get('media_entity_dreambroker.settings')->get('local_images');
      if (!file_exists($directory)) {
        $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
      }

      $local_uri = $this->configFactory->get('media_entity_dreambroker.settings')->get('local_images') . '/' . $matches['videoid'] . '.' . 'png';
      if (file_exists($local_uri)) {
        return $local_uri;
      }
      else {
        $url = 'https://dreambroker.com/channel/' . $matches['channelid'] . '/' . $matches['videoid'] . '/get/poster.png';
        try {
          $image_data = file_get_contents($url);
        }
        catch (\Exception $exception) {
          $image_data = FALSE;
        }

        if ($image_data) {
          return $this->fileSystem->saveData($image_data, $local_uri, FileSystemInterface::EXISTS_REPLACE);
        }
      }
    }

    return parent::getMetadata($media, $attribute_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceFieldConstraints() {
    return ['DreambrokerEmbedCode' => []];
  }

  /**
   * {@inheritdoc}
   */
  public function createSourceField(MediaTypeInterface $type) {
    return parent::createSourceField($type)->set('label', 'Dream Broker Url');
  }

  /**
   * Runs preg_match on embed code/URL.
   *
   * @param \Drupal\media\MediaInterface $media
   *   Media object.
   *
   * @return array|bool
   *   Array of preg matches or FALSE if no match.
   *
   * @see preg_match()
   */
  protected function matchRegexp(MediaInterface $media) {
    $matches = [];

    if (isset($this->configuration['source_field'])) {
      $source_field = $this->configuration['source_field'];
      if ($media->hasField($source_field)) {
        $property_name = $media->{$source_field}->first()->mainPropertyName();
        foreach (static::$validationRegexp as $pattern => $key) {
          if (preg_match($pattern, $media->{$source_field}->{$property_name}, $matches)) {
            return $matches;
          }
        }
      }
    }
    return FALSE;
  }

}
